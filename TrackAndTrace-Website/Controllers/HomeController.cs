﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TrackAndTrace.Models;
using TrackAndTrace_Website.Models;

namespace TrackAndTrace_Website.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //try
            //{
            //    string url = "https://tumdee.sonarinno.com/track/thailandpost/api/SelectItemsByMultipleId";
            //    SelectItems_ByMultipleId regdata = new SelectItems_ByMultipleId
            //    {
            //        cl = "com.sonarinno.tumdee.SelectItemsByMultipleId",
            //        ids = new List<string> { "EE145614148TH", "EE148609439TH", ""}
            //    };

            //    string json = JsonConvert.SerializeObject(regdata);

            //    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            //    httpWebRequest.ContentType = "application/json";
            //    httpWebRequest.Method = "POST";
            //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //    {
            //        streamWriter.Write(json);
            //        streamWriter.Flush();
            //        streamWriter.Close();
            //    }

            //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //    ItemsTracking_Result ItemsTrackingdetail_result = new ItemsTracking_Result();
            //    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //    {
            //        var result = streamReader.ReadToEnd();
            //        ItemsTrackingdetail_result = JsonConvert.DeserializeObject<ItemsTracking_Result>(result);
            //    }

            //    return View(ItemsTrackingdetail_result);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //    ModelState.AddModelError("", "No data");
            //    return View();
            //}

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
