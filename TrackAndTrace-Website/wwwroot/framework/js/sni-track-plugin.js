﻿function getItems(mailIdStr) {
    var mailIds = prepareInput(mailIdStr)

    reqBody = {
        $class: "com.sonarinno.tumdee.SelectItemsByMultipleId",
        ids: mailIds
    };

    var settings = {
        "url": "https://tumdee.sonarinno.com/track/thailandpost/api/SelectItemsByMultipleId",
        data: JSON.stringify(reqBody),
        method: "POST",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false
    }

    $.ajax(settings).done(function (response) {
        handleGetItemsResponse(response);
    });
}

function handleGetItemsResponse(response) {
    $('#mail-id-list').html('');

    if (!response.items.length) {
        $('#mail-id-list').html('<p>No Mail ID found.</p>');

        return;
    }

    response.items.forEach(function (item, idx) {
        var html = '<td><i class="fa fa-check-square" style="font-size:40px"></i></td>';

        html += '<td>';
        // Mail Id
        html += '<h3 class="panel-title">' + item.trackNumber + '</h3>';

        // Receive Status
        html += '<p class="mt0">ผู้รับได้รับแล้ว (13วัน)</p>';

        html += '</td>';

        // Source
        html += '<td style="width:15%"><h3 class="panel-title">' + item.sender.address + '</h3></td>';

        html += '<td style="width:4%"><i class="fa fa-angle-double-right" style = "font-size:40px"></i></td>';

        // Destination
        html += '<td style="width:15%"><h3 class="panel-title">' + item.receiver.address + '</h3></td>';

        // Mail Status
        html += '<td style="width:40%"><p class="mt0">' + item.itemRecords[0].dateTime + ', ' + item.itemRecords[0].location + ', ' + item.itemRecords[0].statusName + '</p></td>';

        html += '<td style="width:5%"><span class="clickable"><i style="font-size:29px" class="fa fa-angle-down"></i></span></td>';

        var divOuter = document.createElement('div');
        divOuter.setAttribute('class', 'mt-2');
        var divMiddle = document.createElement('div');
        divMiddle.setAttribute('class', 'panel panel-success');
        var divInner = document.createElement('div');
        divInner.setAttribute("class", 'panel-heading');
        var tbHeader = document.createElement('table');
        tbHeader.setAttribute('style', 'width:100%');
        var trHeader = document.createElement('tr');
        trHeader.setAttribute('class', 'text-center');

        $(trHeader).html(html);
        $(tbHeader).append(trHeader);
        $(divInner).append(tbHeader);
        $(divMiddle).append(divInner);
        $(divOuter).append(divMiddle);
        $('#mail-id-list').append(divOuter);

        var divBody = document.createElement('div');
        divBody.setAttribute("class", 'panel-body');
        divBody.setAttribute("style", 'display: none;');
        var tbBody = document.createElement('table');
        tbBody.setAttribute('style', 'width:100%');

        $(divBody).append('<h4>ปลายทาง : ' + item.itemRecords[item.itemRecords.length - 1].location + '</h4>');
        item.itemRecords.forEach(function (itemRecord, jdx) {
            var html2 = '<td><i class="fa fa-circle"></i></td>';

            // Time
            html2 += '<td>' + itemRecord.dateTime + '</td>';

            // Status
            html2 += '<td>' + itemRecord.location + ', ' + itemRecord.statusName + '</td>';

            var trBody = document.createElement('tr');
            $(trBody).html(html2);

            $(tbBody).append(trBody);
        });
        $(divBody).append(tbBody);
        $(divMiddle).append(divBody);
    });
}

function prepareInput(mailIdStr) {
    var ids = [];
    var mailIds = mailIdStr.split(',')
    mailIds.forEach(function (mailId, idx) {
        ids[idx] = mailId.replace(/\s/g, '');
    });

    return ids
}

document.getElementById('btn-track').onclick = function () {
    var mailIdStr = $('#input-mailId').val();

    getItems(mailIdStr)
}