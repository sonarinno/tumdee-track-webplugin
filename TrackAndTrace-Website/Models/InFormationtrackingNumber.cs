﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace TrackAndTrace.Models
{
    public class ItemsTracking_Result
    {
        [JsonProperty("$class")]
        public string cl { get; set; }
        public List<ItemsTracking> items { get; set; }
    }

    public class ItemsTracking
    {
        [JsonProperty("$class")]
        public string cl { get; set; }
        public string id { get; set; }
        public string prefix { get; set; }
        public string trackNumber { get; set; }
        //public List<Sender> sender { get; set; }
        //public List<Receiver> receiver { get; set; }
        //public List<ItemRecords> itemRecords { get; set; }
    }

    public class Sender
    {
        [JsonProperty("$class")]
        public string cl { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string id { get; set; }
        public string phoneNumber { get; set; }
    }

    public class Receiver
    {
        [JsonProperty("$class")]
        public string cl { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string id { get; set; }
        public string phoneNumber { get; set; }
    }

    public class ItemRecords
    {
        [JsonProperty("$class")]
        public string cl { get; set; }
        public string status { get; set; }
        public string statusName { get; set; }
        public string dateTime { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public string deliveryDateTime { get; set; }
        public string deliveryName { get; set; }
        public string postCode { get; set; }
    }

    public class SelectItems_ByMultipleId
    {
        [JsonProperty("$class")]
        public string cl { get; set; }
        public List<string> ids { get; set; }
    }
}